This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Commands

- _yarn start_ - Runs the app on [http://localhost:3000](http://localhost:3000)
- _yarn test_ - Runs unit tests

## Demo

A demo is available at [https://sleepy-brushlands-31827.herokuapp.com](https://sleepy-brushlands-31827.herokuapp.com)
