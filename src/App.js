import React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import { ThemeProvider } from './components/ThemeProvider/ThemeProvider';
import { QuestionsListPage } from './pages/QuestionsListPage/QuestionsListPage';
import { QuestionsDetailsPage } from './pages/QuestionsDetailsPage/QuestionsDetailsPage';

const Container = styled.div`
  font-family: 'Raleway', tahoma;
  max-width: 1080px;
  margin: auto;
`;

function App() {
  return (
    <ThemeProvider>
      <Container>
        <Router>
          <Route exact path="/" component={() => <Redirect to="/questions" />} />
          <Route exact path="/questions" component={QuestionsListPage} />
          <Route path="/questions/:id" component={QuestionsDetailsPage} />
        </Router>
      </Container>
    </ThemeProvider>
  );
}

export default App;
