import React from 'react';
import { render, cleanup, waitForElement, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';

import { ThemeProvider } from '../../components/ThemeProvider/ThemeProvider';
import { QuestionsDetailsPage } from './QuestionsDetailsPage';

const mockQuestion = {
  question: 'Dummy Question',
  choices: [{ url: '1' }, { url: '2' }],
};

jest.mock('../../shared/api', () => ({
  api: {
    get: () => Promise.resolve(mockQuestion),
    post: jest.fn(),
  },
}));

describe('QuestionsDetailsPage', () => {
  afterEach(cleanup);

  it('should render question as page title', async () => {
    const { getByTestId } = render(
      <ThemeProvider value={{ theme: { fontSize: {}, colors: {} } }}>
        <QuestionsDetailsPage match={{ params: { id: 'dummy' } }} />
      </ThemeProvider>,
    );

    const titleNode = await waitForElement(() => getByTestId('page-title'));

    expect(titleNode).toHaveTextContent(mockQuestion.question);
  });

  it('should render all the choices', async () => {
    const { getAllByTestId } = render(
      <ThemeProvider value={{ theme: { fontSize: {}, colors: {} } }}>
        <QuestionsDetailsPage match={{ params: { id: 'dummy' } }} />
      </ThemeProvider>,
    );

    const choiceItems = await waitForElement(() => getAllByTestId('choice-item'));

    expect(choiceItems).toHaveLength(mockQuestion.choices.length);
  });

  it('should keep the vote button disabled initially', async () => {
    const { getByTestId } = render(
      <ThemeProvider value={{ theme: { fontSize: {}, colors: {} } }}>
        <QuestionsDetailsPage match={{ params: { id: 'dummy' } }} />
      </ThemeProvider>,
    );

    const voteButton = getByTestId('vote-button');
    expect(voteButton).toHaveAttribute('disabled');
  });

  it('should enable the vote button once a selection is made', async () => {
    const { getByTestId, getAllByTestId } = render(
      <ThemeProvider value={{ theme: { fontSize: {}, colors: {} } }}>
        <QuestionsDetailsPage match={{ params: { id: 'dummy' } }} />
      </ThemeProvider>,
    );

    const choiceItems = await waitForElement(() => getAllByTestId('choice-item'));
    fireEvent.click(choiceItems[0].querySelector(`[id="${mockQuestion.choices[0].url}"]`));
    const voteButton = getByTestId('vote-button');
    expect(voteButton).not.toHaveAttribute('disabled');
  });
});
