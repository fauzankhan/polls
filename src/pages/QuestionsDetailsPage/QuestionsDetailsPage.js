import React, { useEffect, useState, Fragment } from 'react';
import styled from 'styled-components';

import { Button } from '../../components/Button/Button';
import { PageTitle } from '../../components/PageTitle/PageTitle';
import { api } from '../../shared/api';

const Choices = styled.ul`
  list-style: none;
  padding: 0;
`;

const ChoiceItem = styled.li`
  display: flex;
  align-items: center;
  &:not(last-child) {
    border-bottom: 1px solid #eaeaea;
  }
`;

const InputFormGroup = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
`;

const ChoiceTitle = styled.label`
  padding: 16px;
  display: inline-block;
`;

const Votes = styled.div`
  margin: 0 16px;
  min-width: 80px;
`;

const Percentage = styled.div`
  margin: 0 16px;
  min-width: 80px;
  display: none;
  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: block;
  }
`;

const PercentageBar = styled.div`
  width: 100px;
  height: 16px;
  border: 1px solid ${({ theme }) => theme.colors.primary}
  border-radius: 4px;
  position: relative;
  display: none;

  &:after {
    position: absolute;
    content: ' ';
    top: 0;
    left: 0;
    height: 100%;
    width: ${({ progress }) => progress}%;
    background: ${({ theme }) => theme.colors.primary}
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: block;
  }
`;

const getQuestionDetails = (id) => api.get(`/questions/${id}`, { cache: 'no-cache' });

export const QuestionsDetailsPage = ({ match: { params } }) => {
  const [question, setQuestion] = useState({});
  const [totalVotes, setTotalVotes] = useState(null);
  const [selectedOption, setSelectedOption] = useState();
  const [canVote, setCanVote] = useState(false);
  useEffect(() => {
    (async () => {
      const questionDetails = await getQuestionDetails(params.id);
      setQuestion(questionDetails);
    })();
  }, [params.id]);

  useEffect(() => {
    const { choices } = question;
    const votesSum = choices ? choices.reduce((acc, { votes }) => acc + votes, 0) : 0;
    setTotalVotes(votesSum);
  }, [question]);

  const onVote = async () => {
    setCanVote(false);
    const updatedOption = await api.post(selectedOption);

    // getQuestionDetails i.e. GET /questions/id doesn't always give updated values
    // and sometimes even gives a reduced vote count and a different order of choices
    // This behaviour is also reflected in the UI
    const updatedQuestionDetails = await getQuestionDetails(params.id);

    setQuestion(updatedQuestionDetails);
    setCanVote(true);
  };

  const onChoiceSelection = (url) => {
    setCanVote(true);
    setSelectedOption(url);
  };

  const { question: title, choices } = question;

  return (
    <Fragment>
      {title && <PageTitle data-testid="page-title">{title}</PageTitle>}
      <Choices>
        {choices &&
          choices.map(({ choice, url, votes }) => {
            const percentage = totalVotes > 0 ? (votes / totalVotes) * 100 : 0;
            return (
              <ChoiceItem key={url} data-testid="choice-item">
                <InputFormGroup>
                  <input
                    type="radio"
                    name="choice"
                    value={url}
                    id={url}
                    onChange={() => onChoiceSelection(url)}
                  />
                  <ChoiceTitle htmlFor={url}>{choice}</ChoiceTitle>
                </InputFormGroup>
                <Votes>{`${votes} votes`}</Votes>
                <PercentageBar progress={percentage} />
                <Percentage>{`${percentage.toFixed(2)}%`}</Percentage>
              </ChoiceItem>
            );
          })}
      </Choices>
      <Button onClick={onVote} disabled={!canVote} data-testid="vote-button">
        Vote
      </Button>
    </Fragment>
  );
};
