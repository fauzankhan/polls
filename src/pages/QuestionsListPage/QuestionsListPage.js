import React, { useEffect, useState, Fragment } from 'react';
import styled from 'styled-components';

import { PageTitle } from '../../components/PageTitle/PageTitle';
import { QuestionCard } from '../../components/QuestionCard/QuestionCard';
import { api } from '../../shared/api';

const QuesctionsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 24px;

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    grid-template-columns: repeat(3, 1fr);
  }
`;

const QuestionCardWrapper = styled.div`
  position: relative;
  border-radius: 4px;
  overflow: hidden;
`;

export const QuestionsListPage = () => {
  const [questions, setQuestions] = useState([]);
  useEffect(() => {
    (async () => {
      const questionsList = await api.get('/questions');
      setQuestions(questionsList);
    })();
  }, []);

  return (
    <Fragment>
      <PageTitle>Questions</PageTitle>
      <QuesctionsContainer>
        {questions.map(({ question, url, published_at: publishedAt, choices }) => (
          <QuestionCardWrapper key={url}>
            <QuestionCard
              question={question}
              publishedAt={publishedAt}
              url={url}
              numberOfChoices={choices.length}
            />
          </QuestionCardWrapper>
        ))}
      </QuesctionsContainer>
    </Fragment>
  );
};
