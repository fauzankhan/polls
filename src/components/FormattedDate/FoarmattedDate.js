import React, { Fragment } from 'react';

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

export const FormattedDate = ({ date }) => (
  <Fragment>{Intl.DateTimeFormat('en-GB', options).format(new Date(date))}</Fragment>
);
