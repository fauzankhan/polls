import styled from 'styled-components';

export const Button = styled.button`
  padding: 8px 16px;
  background: ${({ theme, disabled }) => (disabled ? theme.colors.grey : theme.colors.primary)}
  color: ${({ theme }) => theme.colors.white}
  font-size: ${({ theme }) => theme.fontSize.medium}
  border-radius: 4px;
  cursor:  pointer;
  border: none;
  outline: none;
`;
