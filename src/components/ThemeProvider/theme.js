export const theme = {
  colors: {
    primary: '#2f3640',
    text: '#3A4649',
    white: '#FFFFFF',
    grey: '#EAEAEA',
  },
  fontSize: {
    small: '12px',
    medium: '16px',
    large: '24px',
  },
  breakpoints: {
    desktop: '768px',
  },
};
