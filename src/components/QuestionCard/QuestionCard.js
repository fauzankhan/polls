import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { FormattedDate } from '../FormattedDate/FoarmattedDate';

const Container = styled.div`
  background: #f1f2f6;
  box-shadow: 0px 0px 8px 0px rgba(177, 179, 175, 1);
  border-radius: 4px;
  padding: 16px;
`;

const Title = styled.h2`
  font-size: 18px;
`;

const Subtitle = styled.p`
  font-size: 14px;
`;

export const QuestionCard = ({ question, url, publishedAt, numberOfChoices }) => (
  <Container>
    <Link to={url}>
      <Title>{question}</Title>
    </Link>
    <Subtitle>
      <FormattedDate date={publishedAt} />
    </Subtitle>
    <Subtitle>{`${numberOfChoices} Choices`}</Subtitle>
  </Container>
);
